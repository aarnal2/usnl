import re
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
from scipy.stats import multivariate_normal
from matplotlib import cm
from matplotlib.colors import ListedColormap

def newColorMap(oldCmap, nColors):
    #https://matplotlib.org/3.1.0/tutorials/colors/colormap-manipulation.html
    big = cm.get_cmap(oldCmap, nColors) #sample a large set of colors from cmap
    newCmap = big(np.linspace(0, 1, nColors)) #
    newCmap[:,3] = np.linspace(0, 1, nColors)
    return ListedColormap(newCmap)
    
def contour(im, n, filename, vmin, vmax, cmap):
    #https://matplotlib.org/api/_as_gen/matplotlib.pyplot.contourf.html#matplotlib.pyplot.contourf
    x = np.linspace(0, im.shape[1], im.shape[1])
    y = np.linspace(0, im.shape[0], im.shape[0])
    X, Y = np.meshgrid(x,y)
    plt.contourf(X, -Y, im, levels = n, cmap=cmap, 
                 origin='image', vmin=vmin, vmax=vmax)
    plt.axis('off')
    plt.savefig(filename)
    plt.clf()
    
def getCoordsFromSVG(fileName):
    file = open(fileName,'r')
    fileContent = file.readline()
    file.close()
    fileContent = fileContent.split('>')
    X=[]
    Y=[]
    viewBox = 0
    for i,line in enumerate(fileContent): 
        coord = 0
        if 'viewBox=' in line:
            viewBox = re.search('viewBox="(.*)"', line)
        if 'circle class=' in line:
            x = re.search('cx="(.*)" cy', line)
            y = re.search('cy="(.*)" r', line)
            coord = [x.group(1),y.group(1)]
        elif 'path class=' in line:
            coord = re.search('"M(.*?)a', line, re.I)
            coord = coord.group(1).split(',')   
        else: continue
        X.append(float(coord[0]))
        Y.append(-float(coord[1]))
    coords = np.vstack((np.array(X), np.array(Y))).T
    return coords, np.array(viewBox[1].split(' '), dtype='float')

def cellDensityMap(coords, viewBox, bandwidth):
    viewBox = viewBox.astype(int)
    xlim = (viewBox[0], viewBox[2])
    ylim = (viewBox[1], -viewBox[3])
    xres = viewBox[2]
    yres = viewBox[3]
    x = np.linspace(xlim[0], xlim[1], xres)
    y = np.linspace(ylim[0], ylim[1], yres)
    X, Y = np.meshgrid(x,y)
    Z = np.zeros((np.c_[X.ravel(), Y.ravel()].shape[0]))
    for i,coord in enumerate(coords):
        # print(i, coord)
        s = np.eye(2)*bandwidth
        k = multivariate_normal(mean=coord, cov=s)
        xxyy = np.c_[X.ravel(), Y.ravel()]
        Z += k.pdf(xxyy)
    return Z.reshape((yres,xres))

def fiberDensityMap(im,windowSize):
    im = 1-(im-np.min(im))/(np.max(im)-np.min(im))
    return gaussian_filter(im,windowSize)