"""
Example 3: one ir–label – one case – multiple level
This example covers how to generate a single density map per level for either 
cells or fibers. This showcases how we can display relative distributions
across density maps.
Refer to Example 1 for introductory details such as color maps 
"""

import visualizeDensity as vD
import matplotlib.pyplot as plt
import numpy as np

newCM = vD.newColorMap('viridis', 1000)     #cmap to use later

"""
Fibers
"""

inFileName1='data/fibers/17-022_nNOS_level23.png'        #specify input file 1
inFileName2='data/fibers/17-022_nNOS_level24.png'        #specify input file 2
outFileName1='data/fibers/contours/17-022_nNOS_level23.svg'   #specify output file 1
outFileName2='data/fibers/contours/17-022_nNOS_level24.svg'   #specify output file 2
window=25       #specify size for gaussian blur: larger values = larger spread
nContours=5     #specify number of contours to draw over density

#Load fiber rasters
fibers1 = plt.imread(inFileName1)[:,:,0]
fibers2 = plt.imread(inFileName2)[:,:,0]

#Compute densities
d1 = vD.fiberDensityMap(fibers1, window)
d2 = vD.fiberDensityMap(fibers2, window)

#Find minimum and maximum value of both densities
low1, high1 = np.min(d1), np.max(d1)
low2, high2 = np.min(d2), np.max(d2)
low, high = np.min((low1,low2)), np.max((high1, high2))

#Draw and save contour to output filename (note vmin and vmax values)
vD.contour(d1, nContours, outFileName1, vmin = low, vmax = high, 
           cmap = newCM)
vD.contour(d2, nContours, outFileName2, vmin = low, vmax = high, 
           cmap = newCM)


"""
Cell bodies
"""
inFileName1='data/cells/17-022_nNOS_level23.svg'        #specify input file 1
inFileName2='data/cells/17-022_nNOS_level24.svg'        #specify input file 2
outFileName1='data/cells/contours/17-022_nNOS_level23.svg'   #specify output file 1
outFileName2='data/cells/contours/17-022_nNOS_level24.svg'   #specify output file 2
bandwidth=100   #specify bandwidth for KDE: larger values = greater smoothing
nContours=5     #specify number of contours to draw over density

#Load svg file containing somata and extract coordinates and viewbox
coords1, viewBox1 = vD.getCoordsFromSVG(inFileName1)
coords2, viewBox2 = vD.getCoordsFromSVG(inFileName2)

#Compute density
d1 = vD.cellDensityMap(coords1, viewBox1, bandwidth)
d2 = vD.cellDensityMap(coords2, viewBox2, bandwidth)

#Find minimum and maximum value of both densities
low1, high1 = np.min(d1), np.max(d1)
low2, high2 = np.min(d2), np.max(d2)
low, high = np.min((low1,low2)), np.max((high1, high2))

#Draw and save contour to output filename (note vmin and vmax values)
vD.contour(d1, nContours, outFileName1, vmin = low, vmax = high, 
           cmap = newCM)
vD.contour(d2, nContours, outFileName2, vmin = low, vmax = high, 
           cmap = newCM)