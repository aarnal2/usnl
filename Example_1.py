"""
Example 1: one ir–label – one case – one level
This example covers how to generate a single density map for either cells or
fibers.
"""

import visualizeDensity as vD
import matplotlib.pyplot as plt
import numpy as np

"""
Before starting, we recommend using vD.newColorMap() to create a new color map
for the fiber and cell contours. We use the original Viridis color map but
with decreasing opacity towards the violet end so the contour map does not 
occlude brain region labels when placed on the atlas template. 
"""
newCM = vD.newColorMap('viridis', 1000)     #cmap to use later


"""
Fibers
Density is estimated via gaussian blur of the raster containing fibers.
"""
inFileName='data/fibers/17-020_nNOS_level23.png'            #specify input file
outFileName='data/fibers/contours/17-020_nNOS_level23.svg'  #specify output file
window=25       #specify size for gaussian blur: larger values = larger spread
nContours=5     #specify number of contours to draw over density

#Load fiber raster - "[:,:,0]" will extract one of the color channels  
fibers = plt.imread(inFileName)[:,:,0]

#Compute density
d = vD.fiberDensityMap(fibers, window)

#Draw and Save Contour to output filename
vD.contour(d, nContours, outFileName, vmin = np.min(d), vmax = np.max(d), 
           cmap = newCM)


"""
Cell bodies
Density is estimated via kernel density estimation (KDE) of somata coordinates.
"""
inFileName='data/cells/17-020_nNOS_level23.svg'             #specify input file
outFileName='data/cells/contours/17-020_nNOS_level23.svg'   #specify output file
bandwidth=100   #specify bandwidth for KDE: larger values = greater smoothing
nContours=5     #specify number of contours to draw over density

#Load svg file containing somata and extract coordinates and viewbox
coords, viewBox = vD.getCoordsFromSVG(inFileName)

#Compute density
d = vD.cellDensityMap(coords, viewBox, bandwidth)

#Draw and Save Contour to output filename
vD.contour(d, nContours, outFileName, vmin = np.min(d), vmax = np.max(d), 
           cmap = newCM)
