"""
Example 2: one ir–label – multiple case – one level
This example covers how to generate a single density map for either cells or
fibers with multiple cases. This showcases how we can combine density maps.
Refer to Example 1 for introductory details such as color maps 
"""

import visualizeDensity as vD
import matplotlib.pyplot as plt
import numpy as np

newCM = vD.newColorMap('viridis', 1000)     #cmap to use later

"""
Fibers
"""

inFileName1='data/fibers/17-020_nNOS_level23.png'        #specify input file 1
inFileName2='data/fibers/17-022_nNOS_level23.png'        #specify input file 2
outFileName='data/fibers/contours/nNOS_level23.svg'   #specify output file
window=25       #specify size for gaussian blur: larger values = larger spread
nContours=5     #specify number of contours to draw over density

#Load fiber rasters
fibers1 = plt.imread(inFileName1)[:,:,0]
fibers2 = plt.imread(inFileName2)[:,:,0]

#Compute individual densities then combine
d1 = vD.fiberDensityMap(fibers1, window)
d2 = vD.fiberDensityMap(fibers2, window)
d = d1+d2       #The manuscript shows additive maps
#d = (d1+d2)/2  #Averaging could be another combination method

#Draw and Save Contour to output filename
vD.contour(d, nContours, outFileName, vmin = np.min(d), vmax = np.max(d), 
           cmap = newCM)


"""
Cell bodies
"""
inFileName1='data/cells/17-020_nNOS_level23.svg'        #specify input file 1
inFileName2='data/cells/17-022_nNOS_level23.svg'        #specify input file 2
outFileName='data/cells/contours/nNOS_level23.svg'   #specify output file
bandwidth=100   #specify bandwidth for KDE: larger values = greater smoothing
nContours=5     #specify number of contours to draw over density

#Load svg file containing somata and extract coordinates and viewbox
coords1, viewBox1 = vD.getCoordsFromSVG(inFileName1)
coords2, viewBox2 = vD.getCoordsFromSVG(inFileName2)

#Compute density
d1 = vD.cellDensityMap(coords1, viewBox1, bandwidth)
d2 = vD.cellDensityMap(coords2, viewBox2, bandwidth)
d = d1+d2       #The manuscript shows additive maps
#d = (d1+d2)/2  #Averaging could be another combination method

#Draw and Save Contour to output filename
vD.contour(d, nContours, outFileName, vmin = np.min(d), vmax = np.max(d), 
           cmap = newCM)
