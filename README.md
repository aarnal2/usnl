This repository contains the code to generate density maps for raster and coordinate data; fibers and somata, respectively, in our case. Our framework assumes fibers and cells have been translated to the rat brain atlas templates from Swanson (Brain Maps 4.0; Swanson, 2018), where fibers are represented by paths and cells are represented by circles in Adobe Illustrator.

---

# Workflow

## Prepare input for program

**Somata coordinates:**

1.	Toggle **ON** all vector graphics representing somata in the rat brain atlas template and hide everything else including atlas template.
2.	The size and color of the circle vector graphic representing a soma has no restriction since the program only extracts the centroid (position) coordinate.
3.	Click **File>Export>Export as...**, set **Format** to **SVG**, check **Use Artboards**, select where to save, and click **Export**

**Fiber raster:**

1.	Toggle **ON** all vector graphics representing fibers in the rat brain atlas template and hide everything else.
2.	The **color** and **weight** of the path representing a fiber should be **black** and **0.3pt**, respectively. Note the weight was chosen in proportion to the atlas template dimensions. Changes to atlas template dimensions may require different path weight. 
3.	Click **File>Export>Export as...**, set **Format** to **PNG**, check **Use Artboards**, select where to save, and click **Export**. Then, in PNG Options, set **Resolution:** to **High (300 ppi)**, set **Anti-aliasing:** to **Type Optimized (Hinted)**, set **Background Color:** to **White**, and click **OK**.

We have chosen the following naming convention for input files to the program:

- caseNumber_ir-label_atlasLevel.png for fibers
- caseNumber_ir-label_atlasLevel.svg for somata

## Generate density map 

Refer to **Example_1.py** to generate density contours for a single case in one level.

Refer to **Example_2.py** to generate *aggregate* density contours for two cases in one level.

Refer to **Example_3.py** to generate *relative* density contours for one case in two levels.

The outputs (density contours) are always in SVG format which can be overlaid back on the rat brain atlas templates.

## Placing density contour in rat brain atlas template:

1.	Open .svg output file in Adobe Illustrator, as well as the corresponding atlas template.
2.	Copy the **axes 1** layer from the density contour file and paste it in the atlas template.
3.	Resize the **axes 1** layer to fit the atlas template's artboard dimensions.
4.	Confirm distribution by toggling **ON** corresponding vector graphics used to generate contour.

---

# Tested with:

-	Python 3.7.6
-	numpy 1.18.1
-	matplotlib 3.1.3
-	scipy  1.4.1
-	re 2.2.1