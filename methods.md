# newColorMap

**visualizeDensity.newColorMap(oldCmap, nColors)**

## Description
Changes opacity of a given color map: one end of the color map is given 100% opacity while the other end is given 0% and all colors in between are given their corresponding opacity interpolated between 100% and 0%.

## Parameters
-    **oldCmap**: string
    -    Any valid color map from [matplotlib](https://matplotlib.org/3.3.0/tutorials/colors/colormaps.html) 


-    **nColors**: integer
    -    Number of colors to sample from original color map for new color map

## Returns

-    **newColorMap**: [ListedColorMap](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.colors.ListedColormap.html#matplotlib.colors.ListedColormap) object
    -    Color map object generated from a list of colors
    
## Sources
-    [Creating Colormaps in Matplotlib](https://matplotlib.org/3.1.0/tutorials/colors/colormap-manipulation.html)



---

# getCoordsFromSVG

**visualizeDensity.getCoordsFromSVG(fileName)**

## Description
Parses SVG file and extracts (x, y) coordinates of circle vector graphics

## Parameters
-    **fileName**: string
    -    Path of the SVG file to parse


## Returns

-    **coords**: (N,2) ndarray
    -    Cell positions (x, y)
    
-    **viewBox**: (4,) ndarray
    -    Reference frame (view box) of .svg file
    


---

# cellDensityMap

**visualizeDensity.cellDensityMap(coords, viewBox, bandwidth)**

## Description
Calculates density via kernel density estimation (KDE) method.

## Parameters
-    **coords**: (N,2) ndarray
    -    Cell positions (x, y)
    
-    **viewBox**: (4,) ndarray
    -    Reference frame (view box) of .svg file
    
-    **bandwidth**: integer
    -    Smoothing parameter for KDE

## Returns

-    **densityMap**: (N,M) ndarray
    -    Density map with same height & width as the SVG's viewBox
    


---


# fiberDensityMap

**visualizeDensity.fiberDensityMap(im,windowSize)**

## Description
Calculates density via gaussian blur.

## Parameters
-    **im**: (N,M) ndarray
    -    Raster of gray–scale image
    
-    **windowSize**: integer
    -    Window size for gaussian blur

## Returns

-    **densityMap**: (N,M) ndarray
    -    Density map with same height & width as the PNG



---

# contour

**visualizeDensity.contour(densityMap, nContours, fileName, vmin, vmax, cmap)**

## Description
Draws and saves an SVG file of the contours of the density map

## Parameters
-    **densityMap**: (N,M) ndarray
    -    Density map used to draw contours

-    **nContours**: integer
    -    Number of contours to draw
    
-    **fileName**: string
    -    Output file name – must include ".svg" as file extention 

-    **vmin**: float
    -    Value that is assigned the last color from the color map
    
-    **vmax**: float
    -    Value that is assigned the first color from the color map
    
-    **cmap**: string or [ListedColorMap](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.colors.ListedColormap.html#matplotlib.colors.ListedColormap) object
    -    Any valid color map from [matplotlib](https://matplotlib.org/3.3.0/tutorials/colors/colormaps.html) or [ListedColorMap](https://matplotlib.org/3.1.0/api/_as_gen/matplotlib.colors.ListedColormap.html#matplotlib.colors.ListedColormap) object


